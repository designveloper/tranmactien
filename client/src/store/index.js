import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import { FETCH_CART_LIST } from '../actions/types';
import cart from '../utils/cart';

export const store = createStore(reducers, applyMiddleware(thunk));
export default store;

store.dispatch({
  type: FETCH_CART_LIST,
  data: cart.getCartList()
})
