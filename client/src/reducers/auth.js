import { LOGIN_USER } from '../actions/types';

export default function reducer(state = { userInfo: {} }, action) {
  switch (action.type) {
    case LOGIN_USER:
      return { ...state, userInfo: action.data };
    default:
      return state;
  }
}
