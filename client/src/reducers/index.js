/**
 * External Dependencies
 */
import { combineReducers } from 'redux';

/**
 * Internal Reducers
 */
import paging from './paging';
import product from './product';
import auth from './auth';
import cart from './cart';

/**
 * Any key provided here will be represented
 * in the Redux Store which can be accessed
 * by Containers via the connect wrapper with
 * the mapStateToProps function parameter.
 */
const rootReducer = combineReducers({
  paging,
  product,
  auth,
  cart
});

// Exporting root reducer
export default rootReducer;
