import { UPDATE_PAGING } from '../actions/types';


export default function reducer(  state = {
  currentPage: 1,
  totalPage: 10,
  hidePrev: false,
  hideNext: false
}, action) {
  switch (action.type) {
    case UPDATE_PAGING:
      let newValue = null;
      if (action.isNext) {
        newValue = state.currentPage + 1
      }
      else {
        newValue = state.currentPage - 1
      }
      return { ...state, currentPage: newValue }
  
    default:
      return state;
  }
}