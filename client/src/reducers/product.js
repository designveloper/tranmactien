import { FETCH_PRODUCTLIST } from '../actions/types';

export default function reducer(state = { productList: [] }, action) {
  switch (action.type) {
    case FETCH_PRODUCTLIST:
      return { ...state, productList: action.data };

    default:
      return state;
  }
}
