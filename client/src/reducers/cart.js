import {
  FETCH_CART_LIST,
  ADD_CART_LIST,
  UPDATE_CART_LIST,
  DELETE_CART_ITEM
} from '../actions/types';

export default function reducer(state = { cartList: [] }, action) {
  switch (action.type) {
    case FETCH_CART_LIST:
      return { ...state, cartList: action.data };
    case ADD_CART_LIST:
      return { ...state, cartList: action.data };
    case UPDATE_CART_LIST:
      return { ...state, cartList: action.data };
    case DELETE_CART_ITEM:
      return { ...state, cartList: action.data };
    default:
      return state;
  }
}
