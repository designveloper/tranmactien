import React, { Component } from 'react';
import { connect } from 'react-redux';
import InputCount from '../InputCount';
import { updateCartList, removeCartItem } from '../../actions/cart';
class RowCartTable extends Component {
  state = {
    qty: this.props.item.qty
  };

  changeCartItem = () => {
    this.props.updateCartList(this.props.item.id, this.state.qty);
    alert('Change successfully!');
  };
  removeCartItem = () => {
    if (window.confirm('Are you sure?')) {
      this.props.removeCartItem(this.props.item.id);
    }
  };
  onChangeInputCountHandle = e => {
    !isNaN(e.target.value) &&
      this.setState({
        qty: e.target.value
      });
  };
  increaseInputCountHandle = e => {
    this.setState(prevState => ({
      qty: parseInt(prevState.qty) + 1
    }));
  };
  decreaseInputCountHandle = e => {
    this.state.qty > 1 &&
      this.setState(prevState => ({
        qty: parseInt(prevState.qty) - 1
      }));
  };
  render() {
    return (
      <tr>
        <td>
          <div className="d-flex">
            <div>
              <img src={this.props.item.imageUrl} alt="" />
            </div>
            <div className="d-flex flex-column justify-content-between p-2">
              <p className="table-cart-name">{this.props.item.name}</p>
              <div>
                <button
                  className="table-cart-btn-control"
                  onClick={this.changeCartItem}
                >
                  Change
                </button>
                <button
                  className="table-cart-btn-control"
                  onClick={this.removeCartItem}
                >
                  Remove
                </button>
              </div>
            </div>
          </div>
        </td>
        <td className="text-center">
          <span
            className="table-cart-color"
            style={{ background: this.props.item.color }}
          />
        </td>
        <td className="text-center">{this.props.item.size}</td>
        <td className="text-center">
          <InputCount
            value={this.state.qty}
            onChange={this.onChangeInputCountHandle}
            increase={this.increaseInputCountHandle}
            decrease={this.decreaseInputCountHandle}
          />
        </td>
        <td className="text-center">
          ${this.props.item.price * this.state.qty}
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => {
  const { cartList } = state.cart;
  return { cartList };
};
export default connect(
  mapStateToProps,
  { updateCartList, removeCartItem }
)(RowCartTable);
