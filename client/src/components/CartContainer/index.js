import React, { Component } from 'react';
import { connect } from 'react-redux';
import RowCartTable from './RowCartTable';
import { fetchCartList } from '../../actions/cart';

class CartContainer extends Component {
  generateCartToTable = () => {
    return this.props.cartList.map(item => (
      <RowCartTable item={item} key={item.id} />
    ));
  };
  getTotalAmount = () => {
    const array = this.props.cartList.map(item => {
      return item.qty * item.price;
    });
    return array.reduce((total, item) => (total += item), 0);
  };
  render() {
    return (
      <div className="container" style={{ minHeight: '60vh' }}>
        <div style={{ height: '50px' }} />
        <div className="row">
          <div className="col-8">
            <h2 className="My-Bag">My Bag</h2>
            {this.props.cartList.length !== 0 ? (
              <div className="table-responsive">
                <table className="table table-cart">
                  <tbody>
                    <tr>
                      <th>Product</th>
                      <th className="text-center">Color</th>
                      <th className="text-center">Size</th>
                      <th className="text-center">Quantity</th>
                      <th className="text-center">Amount</th>
                    </tr>
                    {this.generateCartToTable()}
                  </tbody>
                </table>
              </div>
            ) : (
              <p className="text-center">No item.</p>
            )}
          </div>
          <div className="col-4">
            <h2 className="total">Total</h2>
            <div className="checkout-container ">
              <div className="d-flex justify-content-between">
                <span>Shipping & Handling:</span>
                <span>Free</span>
              </div>
              <div className="d-flex justify-content-between">
                <span>Total product:</span>
                <span>${this.getTotalAmount()}</span>
              </div>
              <hr className="my-2" />
              <div className="d-flex justify-content-between">
                <span>
                  <strong>Subtotal:</strong>
                </span>
                <span>
                  <strong>${this.getTotalAmount()}</strong>
                </span>
              </div>
            </div>
            <button className="btn-checkout mt-3">Check out</button>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { cartList: state.cart.cartList };
};
export default connect(
  mapStateToProps,
  { fetchCartList }
)(CartContainer);
