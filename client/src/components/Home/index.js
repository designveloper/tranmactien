import React, { Component } from 'react';
import HeroBannerContainer from '../HeroBannerContainer';
import SliderBannerContainer from '../SlideBannerContainer';

export default class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <main className="container">
          <HeroBannerContainer />
          <SliderBannerContainer />
        </main>
      </React.Fragment>
    );
  }
}
