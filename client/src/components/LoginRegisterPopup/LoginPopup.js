import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { login } from '../../actions/auth';

const mapStateToProps = state => {
  const { userInfo } = state.auth;
  return { userInfo };
};
class LoginPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      redirect: false
    };
  }
  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  handleSubmit = async event => {
    event.preventDefault();
    const { email, password } = this.state;
    const user = await this.props.login(email, password);
    const { successText, errorText, success } = user;
    if (success) {
      this.setState({
        redirect: true
      });
    } else {
      this.setState({
        successText,
        errorText
      });
    }
  };
  render() {
    const { email, password, successText = '', errorText = '' } = this.state;
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    return (
      <div className="login-popup">
        <button className="login-close" onClick={this.props.onRequestClose}>
          <i className="fa fa-close fa-2x" />
        </button>
        <h2 className="login-popup-title">Login</h2>
        <div className="login-popup-inner">
          {!!successText.length && (
            <div className="alert alert-success">
              <strong>{successText}</strong>
            </div>
          )}
          {!!errorText.length && (
            <div className="alert alert-danger">
              <strong>{errorText}</strong>
            </div>
          )}
          <form onSubmit={this.handleSubmit}>
            <label className="login-label">Email</label>
            <input
              name="email"
              type="email"
              className="login-input"
              placeholder="Enter your name..."
              value={email}
              onChange={this.handleChange}
              required
            />
            <label className="login-label">Password</label>
            <input
              name="password"
              type="password"
              className="login-input"
              placeholder="Enter your password..."
              value={password}
              onChange={this.handleChange}
              required
            />
            <div className="clearfix">
              <div className="float-left">
                <label className="remember-pw">
                  Remember password
                  <input type="checkbox" />
                  <span className="checkmark" />
                </label>
              </div>
              <div className="float-right">
                <Link className="forgot-pw" to="/">
                  Forgot your password?
                </Link>
              </div>
            </div>
            <button type="Submit" className="login-popup-button">
              Login
            </button>
          </form>
        </div>
        <hr style={{ marginTop: '63px' }} />
        <p className="text-center py-3">
          Don't have an account?{' '}
          <a href="" className="link">
            Register
          </a>
        </p>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  { login }
)(LoginPopup);
