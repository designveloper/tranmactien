import React, { Component } from 'react';
import axios from '../../utils/axios';

class RegisterPopup extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    successText: '',
    errorText: ''
  };
  myFormRef = React.createRef();
  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    const { name, email, password } = this.state;
    axios.post('/register', { name, email, password }).then(data => {
      const { successText, errorText } = data.data;
      this.setState({
        successText,
        errorText
      });
      if (successText && successText.length) {
        this.resetForm();
      }
    });
  };
  resetForm = () => {
    this.setState({
      name: '',
      email: '',
      password: ''
    });
  };
  render() {
    const {
      name,
      email,
      password,
      successText = '',
      errorText = ''
    } = this.state;

    return (
      <div className="login-popup">
        <button className="login-close" onClick={this.props.onRequestClose}>
          <i className="fa fa-close fa-2x" />
        </button>
        <h2 className="login-popup-title">Register</h2>
        <div className="login-popup-inner">
          {!!successText.length && (
            <div className="alert alert-success">
              <strong>{successText}</strong>
            </div>
          )}
          {!!errorText.length && (
            <div className="alert alert-danger">
              <strong>{errorText}</strong>
            </div>
          )}
          <form onSubmit={this.handleSubmit} ref={this.myFormRef}>
            <label className="login-label">Name</label>
            <input
              name="name"
              type="text"
              className="login-input"
              placeholder="Enter your name..."
              value={name}
              onChange={this.handleChange}
            />
            <label className="login-label">Email</label>
            <input
              name="email"
              type="email"
              className="login-input"
              placeholder="Enter your email..."
              value={email}
              onChange={this.handleChange}
            />
            <label className="login-label">Password</label>
            <input
              name="password"
              type="password"
              className="login-input"
              placeholder="Enter your password..."
              value={password}
              onChange={this.handleChange}
            />
            <p className="text-center mx-3">
              By creating an account you agree to the{' '}
              <a href="" className="link">
                Terms of Service
              </a>{' '}
              and{' '}
              <a href="" className="link">
                Privacy Policy
              </a>
            </p>
            <button type="Submit" className="login-popup-button">
              Register
            </button>
          </form>
        </div>
        <hr style={{ marginTop: '63px' }} />
        <p className="text-center py-3">
          Do you have an account?{' '}
          <a href="" className="link">
            Log In
          </a>
        </p>
      </div>
    );
  }
}

export default RegisterPopup;
