import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import SideBar from '../SideBar';
import PagingContainer from '../Paging/Paging';
import SelectContainer from '../Select';
import { fetchProductList } from '../../actions/product';

const mapStateToProps = state => {
  const { productList } = state.product;
  return { productList };
};
class ProductListBody extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount = () => {
    this.props.fetchProductList();
  };
  generateProductItem = () => {
    return (
      this.props.productList &&
      this.props.productList.map((item, index) => (
        <ProductItem product={item} key={index} />
      ))
    );
  };

  render() {
    return (
      <div className="container">
        <p className="page-current-url">Ladies / Dresses</p>
        <div className="row no-gutters">
          <div className="col-3">
            <SideBar />
          </div>
          <div className="col-9">
            <div className="clearfix">
              <div className="float-left">
                <div style={{ minWidth: '200px', marginBottom: '10px' }}>
                  <SelectContainer />
                </div>
              </div>
              <div className="float-right">
                <PagingContainer />
              </div>
            </div>
            <div className="product-list">
              <div className="row">{this.generateProductItem()}</div>
            </div>

            <div className="clearfix">
              <div className="float-right">
                <PagingContainer />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ProductItem = props => {
  return (
    <div className="col product-col">
      <div className="product-container">
        <div className="product-image-container">
          <div className="product-image-wrapper">
            <img
              className="product-image"
              src={props.product.imageUrl}
              alt=""
            />

            <Link to={'/product-list/' + props.product._id}>
              <button className="product-cta">+ Quick shop</button>
            </Link>
          </div>
          {props.product.qty === 0 && (
            <span className="product-status">Sold out</span>
          )}
        </div>
        <div className="product-name">{props.product.name}</div>
        <div className="product-price">${props.product.price}</div>
      </div>
    </div>
  );
};

export default connect(
  mapStateToProps,
  { fetchProductList }
)(ProductListBody);
