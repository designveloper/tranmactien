import React from "react";
import ReactDOM from "react-dom";
import "react-input-range/lib/css/index.css";
import InputRange from "react-input-range";

class InputRangeContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      valueInit: {
        min: props.minInit,
        max: props.maxInit
      },
      value: { min: props.min, max: props.max }
    };
  }

  render() {
    return (
      <InputRange
        minValue={this.state.valueInit.min}
        maxValue={this.state.valueInit.max}
        value={this.state.value}
        onChange={value => this.setState({ value })}
      />
    );
  }
}

export default InputRangeContainer;
