import React, { Component } from 'react';
class InputCount extends Component {
  render() {
    const {increase, decrease, ...inputProps} = this.props;
    return (
      <div className="input-group quantity d-inline-flex justify-content-start">
        <div className="input-group-prepend">
          <button className="btn-clean" type="button" onClick={this.props.decrease}>
            -
          </button>
        </div>
        <input
          {...inputProps}
          type="text"
          className="form-control"
          aria-label="Quantity"
        />
        <div className="input-group-append">
          <button className="btn-clean" type="button" onClick={this.props.increase}>
            +
          </button>
        </div>
      </div>
    );
  }
}

export default InputCount;
