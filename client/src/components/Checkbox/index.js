import React, { Component } from 'react';
const inputHiddenStyled = {
  position: 'absolute',
  left: '-9999px',
  visibility: 'hidden'
};
export const SizeCheckbox = props => {
  return (
    <label className="size-checkbox">
      <input
        type={props.type || 'checkbox'}
        name={props.name}
        style={inputHiddenStyled}
        onChange={props.onRequestChange}
        value={props.labelName}
      />
      <span>{props.labelName}</span>
    </label>
  );
};

export const ColorCheckbox = props => {
  return (
    <label className="color-checkbox">
      <input
        type={props.type || 'checkbox'}
        name={props.name}
        style={inputHiddenStyled}
        value={props.colorHex}
        onChange={props.onRequestChange}
      />
      <span style={{ background: props.colorHex }} />
    </label>
  );
};

export const CustomCheckbox = props => {
  return (
    <label className="brand-checkbox">
      <input type="checkbox" style={inputHiddenStyled} />
      <span className="brand-checkbox-label">{props.name}</span>
      <span className="brand-checkbox-sticker" />
    </label>
  );
};
