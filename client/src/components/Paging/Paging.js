import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updatePaging } from '../../actions/paging';

const mapStateToProps = state => {
  const { currentPage, totalPage, hidePrev, hideNext } = state.paging;
  return {
    currentPage,
    totalPage,
    hidePrev,
    hideNext
  };
};

class PagingContainer extends Component {
  onClickChangePaging = isNext => {
    this.props.dispatch(updatePaging(isNext));
  };
  render() {
    return (
      <Paging
        currentPage={this.props.currentPage}
        totalPage={this.props.totalPage}
        hidePrev={this.props.currentPage === 1 ? true : false}
        hideNext={
          this.props.currentPage === this.props.totalPage ? true : false
        }
        onClickChangePaging={this.onClickChangePaging}
      />
    );
  }
}

const Paging = props => {
  return (
    <React.Fragment>
      {!props.hidePrev && (
        <button
          className="paging-btn mr-2"
          onClick={props.onClickChangePaging.bind(null, false)}
        >
          <i className="fa fa-chevron-left" />
        </button>
      )}
      <span>
        {props.currentPage}/{props.totalPage}
      </span>
      {!props.hideNext && (
        <button
          className="paging-btn ml-2"
          onClick={props.onClickChangePaging.bind(null, true)}
        >
          <i className="fa fa-chevron-right" />
        </button>
      )}
    </React.Fragment>
  );
};

export default connect(mapStateToProps)(PagingContainer);
