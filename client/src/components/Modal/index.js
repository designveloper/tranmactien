import React, { Component } from 'react';
import ModalReact from 'react-modal';
import RegisterPopup from '../LoginRegisterPopup/RegisterPopup';
import LoginPopup from '../LoginRegisterPopup/LoginPopup';
import { TYPE_MODAL } from '../../utils/consts';

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    ModalReact.setAppElement('body');
  }
  renderPopupByType(type) {
    let modal;
    switch (type) {
      case TYPE_MODAL.LOGOUT:
        modal = <RegisterPopup onRequestClose={this.props.onRequestClose} />;
        break;
      case TYPE_MODAL.LOGIN:
        modal = <LoginPopup onRequestClose={this.props.onRequestClose} />;
        break;
      default:
        modal = null;
        break;
    }
    return modal;
  }
  render() {
    return (
      <ModalReact
        className="Modal"
        overlayClassName="ModalOverlay"
        isOpen={this.props.isOpen}
      >
        {this.renderPopupByType(this.props.type)}
      </ModalReact>
    );
  }
}
