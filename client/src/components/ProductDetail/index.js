import React, { Component } from 'react';
import { SizeCheckbox, ColorCheckbox } from '../Checkbox';
import InputCount from '../InputCount';
import { addToCart } from '../../actions/cart';
import { connect } from 'react-redux';

class ProductDetailContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      size: '',
      color: '',
      inputCount: 1
    };
  }
  addToCartHandle = () => {
    const { id, imageUrl, name, price } = this.props.product;
    const { size, color, inputCount } = this.state;
    if (!!size.length && !!color.length) {
      this.props.addToCart({
        productId: id,
        imageUrl,
        name,
        price,
        size,
        color,
        qty: inputCount
      });
      alert('Product was successfully added to cart');

    } else {
      alert('You must select size and color.');
    }
  };
  changeCheckboxHandle = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  onChangeInputCountHandle = e => {
    !isNaN(e.target.value) &&
      this.setState({
        inputCount: e.target.value
      });
  };
  increaseInputCountHandle = e => {
    this.setState(prevState => ({
      inputCount: parseInt(prevState.inputCount) + 1
    }));
  };
  decreaseInputCountHandle = e => {
    this.state.inputCount > 1 &&
      this.setState(prevState => ({
        inputCount: parseInt(prevState.inputCount) - 1
      }));
  };
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <p className="page-current-url">Ladies / Dresses</p>
          <div className="row">
            <div className="col-6">
              <div className="row">
                <div className="col-2">
                  <div className="d-flex justify-content-between flex-column h-100">
                    <div className="thumb-img-wrapper">
                      <img
                        src={this.props.product.imageUrl}
                        alt=""
                        className=""
                      />
                    </div>
                    <div className="thumb-img-wrapper">
                      <img
                        src={this.props.product.imageUrl}
                        alt=""
                        className=""
                      />
                    </div>
                    <div className="thumb-img-wrapper">
                      <img
                        src={this.props.product.imageUrl}
                        alt=""
                        className=""
                      />
                    </div>
                    <div className="thumb-img-wrapper">
                      <img
                        src={this.props.product.imageUrl}
                        alt=""
                        className=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-10">
                  <img src={this.props.product.imageUrl} alt="" />
                </div>
              </div>
            </div>
            <div className="col-6">
              <h3 className="product-name">{this.props.product.name}</h3>
              <p className="product-price">${this.props.product.price}</p>
              <label className="product-label">Size</label>
              <div className="d-flex justify-content-start flex-wrap">
                {this.props.product.size &&
                  this.props.product.size.map(item => {
                    return (
                      <SizeCheckbox
                        type="radio"
                        name="size"
                        labelName={item}
                        key={item}
                        onRequestChange={this.changeCheckboxHandle}
                      />
                    );
                  })}
              </div>
              <label className="product-label">Color</label>
              <div className="d-flex justify-content-start flex-wrap">
                {this.props.product.colorHex &&
                  this.props.product.colorHex.map(item => {
                    return (
                      <ColorCheckbox
                        type="radio"
                        name="color"
                        colorHex={item}
                        key={item}
                        onRequestChange={this.changeCheckboxHandle}
                      />
                    );
                  })}
              </div>
              <label className="product-label">Quantity</label>
              <div className="d-block">
                <InputCount
                  value={this.state.inputCount}
                  onChange={this.onChangeInputCountHandle}
                  increase={this.increaseInputCountHandle}
                  decrease={this.decreaseInputCountHandle}
                />
              </div>
              <button
                className="add-to-cart-button"
                onClick={this.addToCartHandle}
              >
                Add to cart
              </button>

              <hr />
            </div>
          </div>
        </div>
        <div style={{ height: '32px' }} />
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  const { cartList } = state.cart;
  return { cartList };
};
export default connect(
  mapStateToProps,
  { addToCart }
)(ProductDetailContainer);
