import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LogoutContainer from '../../../containers/Logout';
import avatar from '../../../images/avatar.jpg';

class AvatarContainer extends Component {
  constructor(props) {
    super(props);
    this.timeOutId = null;
    this.toggleContainer = React.createRef();
    this.state = {
      isOpen: false
    };
    this.onClickOutsideHandle.bind(this);
  }

  componentDidMount() {
    window.addEventListener('click', this.onClickOutsideHandle);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.onClickOutsideHandle);
  }

  onClickOutsideHandle = event => {
    if (
      this.state.isOpen &&
      !this.toggleContainer.current.contains(event.target)
    ) {
      this.setState({ isOpen: false });
    }
  };

  onClickHandle = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  onBlurHandle = () => {
    this.timeOutId = setTimeout(() => {
      this.setState({
        isOpen: false
      });
    });
  };

  onFocusHandle = () => {
    clearTimeout(this.timeOutId);
  };

  render() {
    return (
      <div
        ref={this.toggleContainer}
        className="avatar-container ml-3"
        onFocus={this.onFocusHandle}
        onBlur={this.onBlurHandle}
      >
        <div className="avatar" onClick={this.onClickHandle}>
          <img src={avatar} alt="" />
          <AvatarControl show={this.state.isOpen ? 'show' : ''} />
        </div>
      </div>
    );
  }
}

const AvatarControl = props => {
  return (
    <ul className={`avatar-control ${props.show}`}>
      <li className="avatar-item">
        <Link to="/account-setting" className="avatar-link">
          Account setting
        </Link>
      </li>
      <li className="avatar-item">
        <LogoutContainer />
      </li>
    </ul>
  );
};

export default AvatarContainer;
