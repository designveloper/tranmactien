import React, { Component } from 'react';
import TopHeaderContainer from './TopHeaderContainer';
import BottomHeaderContainer from './BottomHeaderContainer'; 

class Header extends Component {
  render() {
    return (
      <header>
        <TopHeaderContainer/>
        <BottomHeaderContainer/>
      </header>
    );
  }
}

export default Header;
