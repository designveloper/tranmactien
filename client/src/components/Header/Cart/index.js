import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import shopping from '../../../images/shopping.svg';
import { fetchCartList } from '../../../actions/cart';

const mapStateToProps = state => {
  const { cartList } = state.cart;
  return { cartList };
};
class CartContainer extends Component {
  constructor(props) {
    super(props);
    this.timeOutId = null;
    this.toggleContainer = React.createRef();
    this.state = {
      isOpen: false
    };
  }

  componentDidMount() {
    window.addEventListener('click', this.onClickOutsideHandle);
  }
  
  componentWillUnmount() {
    window.removeEventListener('click', this.onClickOutsideHandle);
  }

  onClickOutsideHandle = event => {
    if (
      this.state.isOpen &&
      !this.toggleContainer.current.contains(event.target)
    ) {
      this.setState({ isOpen: false });
    }
  };

  onClickHandle = e => {
    e.preventDefault();
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  onBlurHandle = () => {
    this.timeOutId = setTimeout(() => {
      this.setState({
        isOpen: false
      });
    });
  };

  onFocusHandle = () => {
    clearTimeout(this.timeOutId);
  };

  render() {
    return (
      <div
        ref={this.toggleContainer}
        className="cart-container ml-3"
        onFocus={this.onFocusHandle}
        onBlur={this.onBlurHandle}
      >
        <a href="" className="shopping-group" onClick={this.onClickHandle}>
          <img src={shopping} alt="" />
          <span className="badge shopping-badge">{this.props.cartList.length}</span>
        </a>
        <CartWrapper
          productList={this.props.cartList}
          isShow={this.state.isOpen}
        />
      </div>
    );
  }
}

const CartWrapper = props => {
  return (
    <div className={`cart-wrapper ${props.isShow ? 'show' : ''}`}>
    <div style={{maxHeight: '600px', overflowY: 'auto', overflowX: 'hidden'}}>
      {props.productList.map((item, index) => {
        return (
          <div className="row-wrapper" key={index}>
            <div className="row">
              <div className="col-4">
                <div className="py-2 pl-2">
                  <img
                    className="cart__product-thumb"
                    src={item.imageUrl}
                    alt=""
                  />
                </div>
              </div>
              <div className="col-8">
                <div className="py-2 pr-2">
                  <p className="cart__product-name">{item.name}</p>
                  <div className="clearfix">
                    <p className="float-left cart__product-price">
                      {item.price}
                    </p>
                    <p className="float-right cart__product-group-info">
                      {item.size} - {item.color} - {item.qty}
                      pcs
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}

    </div>
      {props.productList.length ? (
        <Link to="/cart" className="cart-button">
          View Cart
        </Link>
      ) : (
        <p className="cart-button mb-0">No item</p>
      )}
    </div>
  );
};
export default connect(
  mapStateToProps,
  { fetchCartList }
)(CartContainer);
