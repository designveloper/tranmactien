import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Avatar from './Avatar';
import CartContainer from './Cart';
import RegisterButtonContainer from '../Button/RegisterButton';
import LoginButtonContainer from '../Button/LoginButton';
import searchImg from '../../images/general/search.png';
import logo from '../../images/logo.png';
import auth from '../../utils/auth';

class TopHeaderContainer extends Component {
  render() {
    return (
      <div className="top-header">
        <div className="container">
          <div className="row w-100">
            <div className="col-4">
              <div className="search-input-wrapper d-inline-block">
                <input
                  type="text"
                  className="search-input"
                  placeholder="Search"
                />
                <img src={searchImg} alt="" />
              </div>
            </div>
            <div className="col-4">
              <div className="logo">
                <Link to="/">
                  <img src={logo} alt="" />
                </Link>
              </div>
            </div>
            <div className="col-4">
              <div className="login-group">
                {!auth.isUserAuthenticated() ? (
                  <React.Fragment>
                    <RegisterButtonContainer />
                    <LoginButtonContainer />
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <span>{auth.getUserInfo().name}</span>
                    <Avatar />
                  </React.Fragment>
                )}
                <CartContainer />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TopHeaderContainer;
