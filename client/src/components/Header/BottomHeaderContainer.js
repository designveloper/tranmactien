import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const menuList = [ 
  {
    mainMenu: 'Men',
    subMenu: ['Top', 'Bottoms', 'Dresses', 'Jackets', 'Shoes', 'Sale']
  },
  {
    mainMenu: 'Womens',
    subMenu: ['Top', 'Bottoms', 'Dresses', 'Jackets', 'Shoes', 'Sale']
  },
  {
    mainMenu: 'Boys',
    subMenu: ['Top', 'Bottoms', 'Dresses', 'Jackets', 'Shoes', 'Sale']
  },
  {
    mainMenu: 'Girls',
    subMenu: ['Top', 'Bottoms', 'Dresses', 'Jackets', 'Shoes', 'Sale']
  },
];
class BottomHeaderContainer extends Component {
  generateMenu = () => {
    return menuList.map((item) => <MenuItem key={ item.mainMenu } 
                                            label={ item.mainMenu } 
                                            subMenu={ item.subMenu }/> );
  }
  render() {
    return (
      <div className='bottom-header'>
        <ul className='menu'>
          { this.generateMenu() }
        </ul>
      </div>
    );
  }
}

const MenuItem = props => {
  return (
    <li className='menu-item'>
      <a href='' className='menu-link'>{ props.label } <i className='fa fa-angle-down ml-1'></i></a>
      <ul className='submenu'>
        <SubMenuItemContainer subMenu={ props.subMenu }/>
      </ul>
    </li>
  );
}

const SubMenuItemContainer = props => {
  return (
    props.subMenu.map(item => <SubMenuItem key={ item } label={ item }/>)
  );
}

const SubMenuItem = props => {
  return (
    <li className='submenu-item'><Link to='/product-list' className='submenu-link'>{ props.label }</Link></li>
  );
}
export default BottomHeaderContainer;
