import React, { Component } from 'react';
import sliderImg from '../../images/gallery/rectangle-copy-54.png';

export default class SliderBannerContainer extends Component {
  render() {
    return (
      <SliderBanner/>
    );
  }
}

const SliderBanner = (props) => {
  return (
    <div className="slider-banner-container">
      <div className="row">
        <div className="col-3">
          <div className="slider-banner-wrapper">
            <img src={ sliderImg } alt=""/>
            <div className="slider-label-group text-center">
              <h4>Man</h4>
              <hr className="mb-3"/>
              <button className="btn">Shop now</button>
            </div>
          </div>
        </div>
        <div className="col-3">
          <div className="slider-banner-wrapper">
            <img src={ sliderImg } alt=""/>
            <div className="slider-label-group text-center">
              <h4>Man</h4>
              <hr className="mb-3"/>
              <button className="btn">Shop now</button>
            </div>
          </div>
        </div>
        <div className="col-3">
          <div className="slider-banner-wrapper">
            <img src={ sliderImg } alt=""/>
            <div className="slider-label-group text-center">
              <h4>Man</h4>
              <hr className="mb-3"/>
              <button className="btn">Shop now</button>
            </div>
          </div>
        </div>
        <div className="col-3">
          <div className="slider-banner-wrapper">
            <img src={ sliderImg } alt=""/>
            <div className="slider-label-group text-center">
              <h4>Man</h4>
              <hr className="mb-3"/>
              <button className="btn">Shop now</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}