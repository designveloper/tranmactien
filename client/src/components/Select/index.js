import React, { Component } from 'react';
import Select from 'react-select';

class SelectContainer extends Component {
  state = { 
    options: [
      {
        value: "1",
        label: "Popularity"
      },
      {
        value: "2",
        label: "Popularity"
      },
      {
        value: "3",
        label: "Popularity"
      }
    ],

  }
  render() { 
    return <Select isClearable='false' isSearchable='false'  options={ this.state.options } />;
  }
}

export default SelectContainer;