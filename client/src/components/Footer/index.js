import React, { Component } from 'react';
import logo from '../../images/general/logo.png';
class Footer extends Component {
  render() { 
    return (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-2">
              <img src={ logo } alt="" />
            </div>
            <div className="col-8">
              <ul className="footer-list">
                <li className="footer-item"><a href="" className="footer-link">Home</a></li>
                <li className="footer-item"><a href="" className="footer-link">Products</a></li>
                <li className="footer-item"><a href="" className="footer-link">Services</a></li>
                <li className="footer-item"><a href="" className="footer-link">About Us</a></li>
                <li className="footer-item"><a href="" className="footer-link">Help</a></li>
                <li className="footer-item"><a href="" className="footer-link">Contacts</a></li>
              </ul>
            </div>
            <div className="col-2">
              <div className="footer-social">
                <i className="fa fa-twitter"></i>
                <i className="fa fa-facebook"></i>
                <i className="fa fa-instagram"></i>
              </div>
            </div>
          </div>
          <hr/>
          <div className="footer-bottom">
            <div className="clearfix">
              <ul className="footer-list float-left">
                <li className="footer-item"><a href="" className="footer-link">Home</a></li>
                <li className="footer-item"><a href="" className="footer-link">Products</a></li>
                <li className="footer-item"><a href="" className="footer-link">Services</a></li>
                <li className="footer-item"><a href="" className="footer-link">About Us</a></li>
                <li className="footer-item"><a href="" className="footer-link">Help</a></li>
                <li className="footer-item"><a href="" className="footer-link">Contacts</a></li>
              </ul>
              <div className="tnc float-right">
                <a href="" className="footer-link mr-3">Privacy Policy</a>
                <a href="" className="footer-link">Terms &amp; Conditions</a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
 
export default Footer;