import React, { Component } from 'react';
import Modal from 'react-modal';
import RegisterPopup from '../LoginRegisterPopup/RegisterPopup';

class RegisterButtonContainer extends Component {
  state = {
    modalIsOpen: false
  };
  componentWillMount() {
    Modal.setAppElement('body');
  }
  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };
  render() {
    return (
      <React.Fragment>
        <button className="register-btn" onClick={this.openModal}>
          Register
        </button>
        <Modal
          className="Modal"
          overlayClassName="ModalOverlay"
          isOpen={this.state.modalIsOpen}
        >
          <RegisterPopup onRequestClose={this.closeModal} />
        </Modal>
      </React.Fragment>
    );
  }
}

export default RegisterButtonContainer;
