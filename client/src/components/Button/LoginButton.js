import React, { Component } from 'react';
import Modal from 'react-modal';
import LoginPopup from '../LoginRegisterPopup/LoginPopup';

class RegisterButtonContainer extends Component {
  state = {
    modalIsOpen: false
  };
  componentWillMount() {
    Modal.setAppElement('body');
  }
  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };
  render() {
    return (
      <React.Fragment>
        <button className="btn outline-btn" onClick={this.openModal}>
          Login
        </button>
        <Modal
          className="Modal"
          overlayClassName="ModalOverlay"
          isOpen={this.state.modalIsOpen}
        >
          <LoginPopup onRequestClose={this.closeModal} />
        </Modal>
      </React.Fragment>
    );
  }
}

export default RegisterButtonContainer;
