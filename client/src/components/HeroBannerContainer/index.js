import React, { Component } from 'react';

export default class HeroBannerContainer extends Component {
  render() {
    return (
      <HeroBanner srcHero="https://dummyimage.com/1180x513/222/000000"/>
    );
  }
}

const HeroBanner = (props) => {
  return (
    <div className="hero-banner">
      <img src={ props.srcHero } alt="" />
      <h2 className="hero-banner-title">Outfit of the week</h2>
      <button className="btn btn-bigger">Shop now</button>
    </div>
  );
}