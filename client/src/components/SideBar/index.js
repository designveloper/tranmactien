import React, { Component } from 'react';
import AccordionSideBarContainer from '../Accordion/AccordionSideBarContainer';

class SideBar extends Component {
  state = {  }
  render() { 
    return ( 
      <aside>
        <div className="sidebar-group">
          <h2 className="sidebar-label">Categories</h2>
          <hr style={{ 
            width: '20px'
          }}/>
          <ul className="sidebar-list">
            <li className="sidebar-item"><a href="" className="sidebar-link">Rompers / Jumpsuits</a></li>
            <li className="sidebar-item"><a href="" className="sidebar-link">Rompers / Jumpsuits</a></li>
          </ul>
        </div>
        <hr style={{ 
          margin: '50px 0',
          width: '50%'
        }}/>
        <div className="sidebar-group">
          <h2 className="sidebar-label">Filter</h2>
          <AccordionSideBarContainer/>
        </div>
      </aside>
    );
  }
}
export default SideBar;