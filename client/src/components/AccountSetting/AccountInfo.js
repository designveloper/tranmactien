import React, { Component } from 'react';
import auth from '../../utils/auth';
class AccountInfo extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <h3 className="account-title mt-5">Infomation</h3>
        <div className="account-setting-panel account-setting-info mb-5">
          <p className="mb-0">
            <b>Name</b>
          </p>
          <p>{auth.getUserInfo().name}</p>

          <p className="mb-0">
            <b>Email</b>
          </p>
          <p>{auth.getUserInfo().email}</p>
        </div>
      </React.Fragment>
    );
  }
}

export default AccountInfo;
