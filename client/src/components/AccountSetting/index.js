import React, { Component } from 'react';
import AccountInfo from './AccountInfo';
import AccountChangePassword from './AccountChangePassword';

const CURRENT_TAB = {
  INFO: 0,
  CHANGE_PASSWORD: 1
};
class AccountSetting extends Component {
  state = {
    currentTab: CURRENT_TAB.INFO,
    renderTab: {
      [CURRENT_TAB.INFO]: {
        name: 'Account Setting',
        renderTarget: <AccountInfo />
      },
      [CURRENT_TAB.CHANGE_PASSWORD]: {
        name: 'Change Password',
        renderTarget: <AccountChangePassword />
      }
    }
  };
  onChangeTab = e => {
    const tab = e.target.getAttribute('data-tab');
    this.setState({
      currentTab: tab
    });
  };
  render() {
    const { currentTab, renderTab } = this.state;
    return (
      <div className="container">
        <div className="row no-gutters account-setting">
          <div className="col-3">
            <aside className="account-setting-sidebar">
              <h3>My Account</h3>
              <ul className="account-setting-list">
                {Object.keys(renderTab).map((key, index) => {
                  return (
                    <li className="account-setting-item mt-3" key={index}>
                      <button
                        className={`account-setting-btn ${
                          currentTab == key ? 'active' : ''
                        }`}
                        data-tab={key}
                        onClick={this.onChangeTab}
                      >
                        {renderTab[key].name}
                      </button>
                    </li>
                  );
                })}
              </ul>
            </aside>
          </div>
          <div className="col-9">{renderTab[currentTab].renderTarget}</div>
        </div>
      </div>
    );
  }
}

export default AccountSetting;
