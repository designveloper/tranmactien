import React, { Component } from 'react';
import axios from '../../utils/axios';

class AccountChangePassword extends Component {
  state = {
    success: '',
    error: ''
  };
  onChangeHandle = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  submitForm = e => {
    e.preventDefault();
    const { currentPassword, newPassword, retypePassword } = this.state;
    if (newPassword.length < 6) {
      this.setState({
        error: 'Password must have at least 6 characters.'
      });
    } else if (retypePassword !== newPassword) {
      this.setState({
        error: 'Retype password did not match.'
      });
    } else {
    axios
      .post('/change-password', {
        currentPassword,
        newPassword,
        retypePassword
      })
      .then(response => {
        const { success = '', error = ''} = response.data;
        this.setState({
          success,
          error
        });
      });
    }
  };
  render() {
    const { success = '', error = '' } = this.state;
    return (
      <React.Fragment>
        <h3 className="account-title mt-5">Change password</h3>
        <div className="account-setting-panel account-setting-change-pw mb-5">
          {!!success.length && (
            <div className="alert alert-success">
              <strong>{success}</strong>
            </div>
          )}
          {!!error.length && (
            <div className="alert alert-danger">
              <strong>{error}</strong>
            </div>
          )}
          <form onSubmit={this.submitForm}>
            <label>CURRENT PASSWORD</label>
            <input
              name="currentPassword"
              type="password"
              className="form-control mb-3"
              onChange={this.onChangeHandle}
              required
            />
            <label>NEW PASSWORD</label>
            <input
              name="newPassword"
              type="password"
              className="form-control mb-3"
              onChange={this.onChangeHandle}
              required
            />
            <label>RE-ENTER NEW PASSWORD</label>
            <input
              name="retypePassword"
              type="password"
              className="form-control mb-3"
              onChange={this.onChangeHandle}
              required
            />
            <div className="text-right mt-3">
              <button type="submit">Save</button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default AccountChangePassword;
