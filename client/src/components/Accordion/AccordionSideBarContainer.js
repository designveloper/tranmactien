import React, { Component } from 'react';
import { SizeCheckbox, ColorCheckbox, CustomCheckbox } from '../Checkbox';
import InputRangeContainer from '../InputRangeContainer';

class AccordionSideBarContainer extends Component {
  state = {};
  render() {
    return (
      <ul className='sidebar-list'>
        <AccordionSideBarItemContainer name='Size'>
          <div className='d-flex justify-content-start flex-wrap'>
            <SizeCheckbox labelName='S' />
            <SizeCheckbox labelName='M' />
            <SizeCheckbox labelName='L' />
          </div>
        </AccordionSideBarItemContainer>
        <AccordionSideBarItemContainer name='Color'>
          <div className='d-flex justify-content-start flex-wrap'>
            <ColorCheckbox colorHex='#ff5f6d' value='abc' />
            <ColorCheckbox colorHex='green' value='abc' />
            <ColorCheckbox colorHex='#222' value='abc' />
          </div>
        </AccordionSideBarItemContainer>
        <AccordionSideBarItemContainer name='Brands'>
          <div className='d-flex justify-content-start flex-wrap'>
            <CustomCheckbox name='Zara' />
            <CustomCheckbox name='HM' />
            <CustomCheckbox name='Dior' />
          </div>
        </AccordionSideBarItemContainer>
        <AccordionSideBarItemContainer name='Price'>
          <div className='d-flex justify-content-start flex-wrap'>
            <div className='py-4 w-100'>
              <InputRangeContainer
                minInit={0}
                maxInit={200}
                min={10}
                max={100}
              />
            </div>
          </div>
        </AccordionSideBarItemContainer>
        <AccordionSideBarItemContainer name='Available'>
          <div className='d-flex justify-content-start flex-wrap'>
            <CustomCheckbox name='In-store' />
            <CustomCheckbox name='Out of stock' />
          </div>
        </AccordionSideBarItemContainer>
      </ul>
    );
  }
}

class AccordionSideBarItemContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      isActive: false
    };
  }

  onClickHandle = () => {
    this.setState(prevState => ({
      isActive: !prevState.isActive
    }));
  };

  render() {
    return (
      <AccordionSideBarItem
        name={this.state.name}
        isActive={this.state.isActive}
        onClickHandle={this.onClickHandle.bind(this)}
        children={this.props.children}
      />
    );
  }
}

const AccordionSideBarItem = props => {
  return (
    <li className='sidebar-item sidebar-item-accordion'>
      <button
        className={`sidebar-accordion ${props.isActive ? 'active' : ''}`}
        onClick={props.onClickHandle}
      >
        {props.name} <i className='fa fa-chevron-down float-right mt-1' />
      </button>
      <div
        className={`sidebar-item-accordion-content ${
          props.isActive ? 'show' : ''
        }`}
      >
        {props.children}
      </div>
    </li>
  );
};
export default AccordionSideBarContainer;
