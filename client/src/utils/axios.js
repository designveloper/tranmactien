import axios from 'axios';
import { BASE_URL } from '../utils/consts';
import auth from '../utils/auth';
export default axios.create({
  baseURL: BASE_URL,
  headers: { Authorization: 'Bearer ' + auth.getUserInfo().token }
});
