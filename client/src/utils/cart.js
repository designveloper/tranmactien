import uniqid from 'uniqid';
export default class cart {
  static getCartList() {
    return JSON.parse(localStorage.getItem('cartList')) || [];
  }
  static addToCart(product) {
    const cartList = this.getCartList();
    const indexProduct = cartList.findIndex(item => {
      return (
        item.productId === product.productId &&
        item.color === product.color &&
        item.size === product.size
      );
    });
    if (indexProduct > -1) {
      cartList[indexProduct].qty += product.qty;
    } else {
      product = { ...product, id: uniqid() };
      cartList.push(product);
    }
    localStorage.setItem('cartList', JSON.stringify(cartList));
    return cartList;
  }
  static removeFromCart(id) {
    const cartList = this.getCartList();
    const removedCartList = cartList.filter(item => {
      return item.id !== id;
    });
    localStorage.setItem('cartList', JSON.stringify(removedCartList));
    return removedCartList;
  }
  static updateQtyProduct(id, qty) {
    const cartList = this.getCartList();
    cartList.forEach(item => {
      if (item.id === id) {
        item.qty = qty;
      }
    });
    localStorage.setItem('cartList', JSON.stringify(cartList));
    return cartList;
  }
}
