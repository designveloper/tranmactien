import React, { Component } from 'react';
import Header from '../../components/Header';
import AccountSetting from '../../components/AccountSetting';
import Footer from '../../components/Footer';

class AccountSettingPage extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <AccountSetting />
        <Footer />
      </React.Fragment>
    );
  }
}

export default AccountSettingPage;
