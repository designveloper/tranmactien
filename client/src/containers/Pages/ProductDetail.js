import React, { Component } from 'react';
import ProductDetailContainer from '../../components/ProductDetail';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import axios from '../../utils/axios';

class ProductListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {}
    };
  }
  componentDidMount = async () => {
    const productId = this.props.match.params.id;
    const response = await axios.get(`/product-list/${productId}`);
    const { _id, name, imageUrl, price, size, color } = response.data;
    this.setState({
      product: {
        id: _id,
        imageUrl,
        name,
        price,
        size,
        colorHex: color
      }
    });
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <ProductDetailContainer product={this.state.product} />
        <Footer />
      </React.Fragment>
    );
  }
}
export default ProductListPage;
