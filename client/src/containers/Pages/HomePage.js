import React, { Component } from 'react';
import Header from '../../components/Header';
import Home from '../../components/Home';
import Footer from '../../components/Footer';

class HomePage extends Component {
  render() { 
    return (
      <React.Fragment>
        <Header />
        <Home />
        <Footer />
      </React.Fragment>
    );
  }
}

export default HomePage;