import React, { Component } from 'react';
import Header from '../../components/Header';
import CartContainer from '../../components/CartContainer';
import Footer from '../../components/Footer';

class CartPage extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <CartContainer />
        <Footer />
      </React.Fragment>
    );
  }
}

export default CartPage;
