import React, { Component } from 'react';
import Header from '../../components/Header';
import ProductListBody from '../../components/ProductListBody';
import Footer from '../../components/Footer';

class ProductListPage extends Component {
  render() { 
    return (
      <React.Fragment>
        <Header />
        <ProductListBody />
        <Footer />
      </React.Fragment>
    );
  }
}
export default ProductListPage;