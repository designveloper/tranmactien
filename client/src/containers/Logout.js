import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import auth from '../utils/auth';
import axios from '../utils/axios';

class LogoutContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };
  }
  onRequestLogout = e => {
    e.preventDefault();
    const { email } = auth.getUserInfo();
    auth.deauthenticateUser();
    axios.post('/logout', { email }).then(reponse => {
      this.setState({
        redirect: true
      });
    }).catch(err => {
      console.log(err.message);
    });
  };
  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
  };
  render() {
    return (
      <React.Fragment>
        {this.renderRedirect()}
        <Logout logout={this.onRequestLogout} />
      </React.Fragment>
    );
  }
}

const Logout = props => {
  return (
    <a href="" className="avatar-link" onClick={props.logout}>
      Logout
    </a>
  );
};

export default LogoutContainer;
