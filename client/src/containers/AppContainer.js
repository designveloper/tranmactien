/**
 * External Dependencies
 */
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

/**
 * Internal Dependencies
 */
import ScrollTop from "../utils/ScrollTop";

// Components/Containers
import HomePage from "./Pages/HomePage";
import ProductListPage from "./Pages/ProductListPage";
import ProductDetail from "./Pages/ProductDetail";
import CartPage from "./Pages/CartPage";
import AccountSettingPage from "./Pages/AccountSettingPage";


class AppContainer extends Component {
  componentDidMount() {}

  render() {
    return (
      <Router>
        <ScrollTop>
          <div>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route exact path="/product-list" component={ProductListPage} />
              <Route exact path="/product-list/:id" component={ProductDetail} />
              <Route exact path="/cart" component={CartPage} />
              <Route exact path="/account-setting" component={AccountSettingPage} />
            </Switch>
          </div>
        </ScrollTop>
      </Router>
    );
  }
}

// Export
export default AppContainer;
