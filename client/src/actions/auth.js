import { LOGIN_USER } from './types';
import auth from '../utils/auth';
import axios from '../utils/axios';
export const login = (email, password) => async dispatch => {
  const response = await axios.post('/login', { email, password });
  const { success = false, token } = response.data;
  if (success) {
    const { name } = response.data.user;
    auth.setAuthenticateUser({ token, email, name });
    dispatch({
      type: LOGIN_USER,
      data: response.data
    });
  }
  return response.data;
};