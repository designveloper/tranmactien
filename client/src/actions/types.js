/**
 * @file: types.js
 * @desc: contains all the types that are being used for actions
 */

 export const FETCH_USER = 'FETCH_USER';
 export const UPDATE_PAGING = 'UPDATE_PAGING';
 export const FETCH_PRODUCTLIST = 'FETCH_PRODUCTLIST';
 export const LOGIN_USER = 'LOGIN_USER';
 export const FETCH_CART_LIST = 'FETCH_CART_LIST';
 export const ADD_CART_LIST = 'ADD_CART_LIST';
 export const UPDATE_CART_LIST = 'UPDATE_CART_LIST';
 export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';
 export const FETCH_USER_FROM_LOCAL = 'FETCH_USER_FROM_LOCAL';
 