import { FETCH_PRODUCTLIST } from './types';
import axios from '../utils/axios';
export const fetchProductList = () => async dispatch => {
  const { data } = await axios.get('api/product');
  dispatch({
    type: FETCH_PRODUCTLIST,
    data
  });
};
