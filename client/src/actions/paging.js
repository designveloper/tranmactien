import { UPDATE_PAGING } from './types';

export const updatePaging = isNext => {
  return {
    type: UPDATE_PAGING,
    isNext
  };
};
