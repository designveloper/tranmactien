import {
  FETCH_CART_LIST,
  ADD_CART_LIST,
  UPDATE_CART_LIST,
  DELETE_CART_ITEM
} from './types';
import cart from '../utils/cart';
export const fetchCartList = () => dispatch => {
  const cartList = cart.getCartList();
  dispatch({
    type: FETCH_CART_LIST,
    data: cartList
  });
  return cartList;
};

export const addToCart = product => dispatch => {
  const cartList = cart.addToCart(product);
  dispatch({
    type: ADD_CART_LIST,
    data: cartList
  });
  return cartList;
};

export const updateCartList = (id, qty) => dispatch => {
  const cartList = cart.updateQtyProduct(id, qty);
  dispatch({
    type: UPDATE_CART_LIST,
    data: cartList
  });
  return cartList;
};
export const removeCartItem = id => dispatch => {
  const cartList = cart.removeFromCart(id);
  dispatch({
    type: DELETE_CART_ITEM,
    data: cartList
  });
  return cartList;
};
