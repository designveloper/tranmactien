/**
 * External Dependencies
 */
import jwt from 'jsonwebtoken';
import LocalStrategy from 'passport-local';
import User from '../data/models/Users';
import { JWT_KEY } from '../commons/consts';
/**
 * SerializeUser is used to provide some identifying token that can be saved
 * in the users session.  We traditionally use the 'ID' for this.
 */

export default passport => {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  /**
   * The counterpart of 'serializeUser'.  Given only a user's ID, we must return
   * the user object.  This object is placed on 'req.user'.
   */
  passport.deserializeUser((user, done) => {
    User.findById(user.id, (err, userFound) => {
      done(err, userFound);
    });
  });

  /**
   * Instructs Passport how to authenticate a user using a locally saved email
   * and password combination. This strategy is called whenever a user attempts to
   * log in. We first find the user model in MongoDB that matches the submitted email,
   * then check to see if the provided password matches the saved password. There
   * are two obvious failure points here: the email might not exist in our DB or
   * the password might not match the saved one. In either case, we call the 'done'
   * callback, including a string that messages why the authentication process failed.
   * This string is provided back to the GraphQL client.
   */
  passport.use(
    'local-login',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        passReqToCallback: true
      },
      async (req, email = '', password = '', done) => {
        const userFound = await User.findOne({ email: email.toLowerCase() });

        // If User not found or password isn't correct, return false
        try {
          if (!userFound || !userFound.comparePassword(password)) {
            const error = new Error('Incorrect email or password');
            error.name = 'IncorrectCredentialsError';
            return done(error);
          }
          const payload = {
            userId: userFound._id,
            name: userFound.name
          };
          // create a token string
          const token = await jwt.sign(payload, JWT_KEY);
          const data = {
            name: userFound.name,
            email: userFound.email
          };
  
          const update = await User.findOneAndUpdate({ _id: userFound._id }, { $set: { tokenLogin: token } });
          return done(null, token, data);
        }
        catch (err) {
          return done(err, null, err);
        }
      }
    )
  );
};
