/**
 * @file: dev.js
 * @desc: contains developer keys for any external 3rd party
 * package or library we use
 *
 * @see: prod.js for production keys
 */

/**
 * Local Variables
 */
const MONGO_URI = 'mongodb://tientran:123123AAA@ds127342.mlab.com:27342/intern-project';
const COOKIE_SECRET = 'helloworld';

export default {
	MONGO_URI,
	COOKIE_SECRET
};
