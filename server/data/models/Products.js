import mongoose from 'mongoose';
import productList from '../products.json';

const ProductSchema = new mongoose.Schema({
  name: String,
  price: { type: Number, default: 0 },
  qty: { type: Number, default: 0 },
  color: [{ type: String }],
  size: [{ type: String }],
  brand: String,
  category: String,
  imageUrl: String
});

const ProductModel = mongoose.model('Products', ProductSchema);

export function initDB() {
  const Products = mongoose.model('Products', ProductSchema);
  productList.forEach(item => {
    const { name, price, qty, color, size, brand, category, imageUrl } = item;
    var productInstance = new Products({
      name,
      price,
      qty,
      color,
      size,
      brand,
      category,
      imageUrl
    });
    productInstance.save(function(err) {
      if (err)
        // ...
        console.error(err.name);
    });
  });
}

export default ProductModel;
