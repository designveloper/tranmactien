import { USER_ROLE } from '../../commons/consts';
import mongoose from 'mongoose';
import md5 from 'md5';

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: Number,
    required: true,
    default: USER_ROLE.USER
  },
  tokenLogin: {
    type: String,
    allowNull: true,
    default: null
  }
});
UserSchema.methods.comparePassword = function(password) {
  return this.password === md5(password);
};
UserSchema.pre('save', function(next) {
  const user = this;
  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();
  const hash = md5(user.password);
  user.password = hash;
  next();
});
UserSchema.pre('update', function(next) {
  this.update({}, { $set: { password: md5(this.getUpdate().$set.password) } });
  next();
});

const UserModel = mongoose.model('Users', UserSchema);

export default UserModel;
