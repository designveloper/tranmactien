/* eslint-disable no-console */

/**
 * External Dependencies
 */
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import cookieSession from 'cookie-session';
import passport from 'passport';
import path from 'path';
import cors from 'cors';
import flash from 'connect-flash';
import './data/models/Users';
import './data/models/Products';
import passportConfig from './services/auth';
/**
 * Interal Dependencies
 */
import keys from './config/keys';

/**
 * Mongoose Connection
 */
mongoose.set('debug', true);
mongoose.Promise = global.Promise;

// Connect with the provided mongo URI
mongoose.connect(
  keys.MONGO_URI,
  {
    useMongoClient: true
  }
);

// If connection fails, stop and show error
mongoose.connection.on('error', err => {
  console.error(`\nMongoose Connection Error: \n${err}`); console.error(`\nMongoose Connection Error: \n${err}`);
  process.exit(1);
});
mongoose.connection.once('open', function() {
  console.log('>> Connected!');
})

/**
 * Local Variables
 */
const app = express();
const PORT = process.env.PORT || 8080;

// body parser middleware
app.use(bodyParser.json());

app.use(cookieParser(keys.COOKIE_SECRET));
app.use(cors());

// enable cookies here
// app.use(
//   cookieSession({
//     resave: false,
//     maxAge: 30 * (24 * 60 * 60 * 1000), // keep cookie for 30 days
//     keys: [keys.COOKIE_SECRET] // encrypt cookie,
//   })
// );
app.use(
  session({
    secret: 'tien',
    saveUninitialized: true,
    resave: true,
  })
);
passportConfig(passport);
/**
 * Routes for App
 */
import routes from './routes';
routes(app);

/**
 * Handling React with Express
 *Users
 * In production, when the user makes a request for whicUsersh the
 * routes above do not handle, we're going to send back Usersfiles
 * from build as if the user is making a client side reqUsersuest.
 *Users
 * We return "index.html" file instead and from the pathUsers specified, the
 * react router will know what to show.
 */

if (process.env.NODE_ENV === 'production') {
  // Serve Production assets like css, js
  app.use(express.static('client/build'));

  // Serve index.html if it doesn't match any routes above
  app.get('/', (req, res) => {
    res.sendFile(
      path.resolve(__dirname, '../', 'client', 'build', 'index.html')
    );
  });
}

// Listen on PORT
app.listen(PORT, () => {
  console.log(`App is running on http://localhost:${PORT}`);
});
