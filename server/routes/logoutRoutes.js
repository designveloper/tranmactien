import User from '../data/models/Users';

const logoutRoutes = app => {
  app.post('/logout', async (req, res, next) => {
    const { email } = req.body;
    try {
      await User.update({ email }, { $set: { token: null } });
      res.status(200).end();
    } catch (err) {
      console.log(err.message);
    }
  });
};

export default logoutRoutes;
