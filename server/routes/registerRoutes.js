import UserModel from '../data/models/Users';
import { registerValidator } from '../middlewares/validate';
import { validationResult } from 'express-validator/check';

const registerRoutes = app => {
  app.post('/register', registerValidator, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.send({
        errorText: errors.array()[0].msg
      });
    } else {
      const { name, email, password } = req.body;
      console.log(name, email, password);
      
      try {
        UserModel.create({ name, email, password }).then(data => {
        }).catch(err => {console.log(err)});
        res.send({
          successText: 'Register Successfully!'
        });
      } catch (err) {
        res.send(err.message);
      }
    }
  });
};

export default registerRoutes;
