/**
 * @file: index.js
 * @desc: Exports all the routing files for the
 * backend into an object
 */

/**
 * Internal Dependencies
 */
import userRoutes from './userRoutes';
import productRoutes from './productRoutes';
import loginRoutes from './loginRoutes';
import registerRoutes from './registerRoutes';
import logoutRoutes from './logoutRoutes';

/**
 * List of all routes handled by Express
 * @param {Object} app: Express Object
 */
const routes = (app) => {
	userRoutes(app);
	productRoutes(app);
	loginRoutes(app);
	registerRoutes(app);
	logoutRoutes(app);
};

// Export
export default routes;
