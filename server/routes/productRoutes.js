import ProductModel, { initDB } from '../data/models/Products';

const productRoutes = app => {

  /* remove this function on production */
  app.get('/api/product/create', (req, res) => {
    initDB();
    res.send('Done!');
  });

  app.get('/api/product', async (req, res) => {
    const data = await ProductModel.find({});
    res.json(data);
  });
  app.get('/product-list/:id', async (req, res) => {
    const productId = req.params.id;
    console.log(productId);
    const data = await ProductModel.findOne({
      _id: productId
    });
    res.json(data);
  });
};

export default productRoutes;
