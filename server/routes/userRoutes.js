import UserModel from '../data/models/Users';
import userIsLoggedIn from '../middlewares/userIsLoggedIn';
import { changePasswordValidator } from '../middlewares/validate';
import { validationResult } from 'express-validator/check';

const userRoutes = app => {
  app.post(
    '/change-password',
    userIsLoggedIn,
    changePasswordValidator,
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        res.send({
          error: errors.array()[0].msg
        });
      } else {
				const password = req.body.newPassword;
        try {
          await UserModel.update({ _id: req.user._id }, { $set: { password } });
          res.send({
            success: 'Change password successfully!'
          });
        } catch (err) {
          res.send({ error: err.message });
        }
      }
    }
  );
};

export default userRoutes;
