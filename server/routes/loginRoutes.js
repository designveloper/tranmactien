import passport from 'passport';
import { validationResult } from 'express-validator/check';
import { loginValidator } from '../middlewares/validate';

const loginRoutes = app => {
  app.post('/login', loginValidator, (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.send({
        errorText: errors.array()[0].msg
      });
    } else {
      return passport.authenticate('local-login', (err, token, userData) => {
        if (err) {
          if (err.name === 'IncorrectCredentialsError') {
            return res.json({
              success: false,
              errorText: err.message
            });
          }

          return res.json({
            success: false,
            errorText: err.message
          });
        }

        return res.json({
          success: true,
          successText: 'You have successfully logged in!',
          token,
          user: userData
        });
      })(req, res, next);
    }
  });
};

export default loginRoutes;
