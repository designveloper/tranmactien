import md5 from 'md5';
import { body } from 'express-validator/check';
import UserModel from '../data/models/Users';

export const registerValidator = [
  body('name')
    .exists()
    .withMessage('Name is required.')
    .not()
    .isEmpty()
    .withMessage('Name is required.'),
  body('email')
    .exists()
    .withMessage('Email is required.')
    .not()
    .isEmpty()
    .withMessage('Email is required.')
    .isEmail()
    .withMessage('Invalid email format')
    .custom(async (value, { req }) => {
      let user = await UserModel.findOne({ email: value });
      if (!user) {
        return value;
      } else {
        throw new Error('Email already exists');
      }
    }),
  body('password')
    .exists()
    .withMessage('Password is required.')
    .not()
    .isEmpty()
    .withMessage('Password is required.')
    .isLength({ min: 6 })
    .withMessage('Password must have at least 6 characters.')
];

export const loginValidator = [
  body('email')
    .exists()
    .withMessage('Email is required.')
    .not()
    .isEmpty()
    .withMessage('Email is required.')
    .isEmail()
    .withMessage('Invalid email format'),
  body('password')
    .exists()
    .withMessage('Password is required.')
    .not()
    .isEmpty()
    .withMessage('Password is required.')
];
export const changePasswordValidator = [
  body('currentPassword')
    .exists()
    .withMessage('Password is required.')
    .not()
    .isEmpty()
    .withMessage('Password is required.')
    .custom(async (value, { req }) => {
      const userVerify = await UserModel.findOne({ _id: req.user._id });
      if (md5(value) !== userVerify.password) {
        throw new Error('Wrong password!');
      } else {
        return value;
      }
    }),
  body('newPassword')
    .exists()
    .withMessage('Password is required.')
    .not()
    .isEmpty()
    .withMessage('Password is required.')
    .isLength({ min: 6 })
    .withMessage('Password must have at least 6 characters.'),
  body('retypePassword')
    .exists()
    .withMessage('Retype password is required.')
    .not()
    .isEmpty()
    .withMessage('Retype password is required.')
    .custom((value, { req }) => {
      if (value === req.body.newPassword) {
        return value;
      } else {
        throw new Error('Retype password did not match.');
      }
    })
];
