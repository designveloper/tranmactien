import jwt from 'jsonwebtoken';
import User from '../data/models/Users';
import { JWT_KEY } from '../commons/consts';
export default async function(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).end();
  }

  // get the last part from a authorization header string like "bearer token-value"
  const token = req.headers.authorization.split(' ')[1];
  // decode the token using a secret key-phrase
  try {
    const decoded = await jwt.verify(token, JWT_KEY);
    const userId = decoded.userId;
    const userVerify = await User.findOne({ _id: userId });
    if (userVerify) {
      req.user = userVerify;
      return next();
    } else {
      req.user = null;
      return res.status(401).end();
    }
  } catch (err) {
    console.log(err.message);
    return res.status(401).end();
  }
}
